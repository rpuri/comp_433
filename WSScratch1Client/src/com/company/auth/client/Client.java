package com.company.auth.client;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import demo.spring.service.HelloSEI;
public final class Client {
	private Client() {
    } 

    public static void main(String args[]) throws Exception {

    	JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();

    	factory.getInInterceptors().add(new LoggingInInterceptor());
    	factory.getOutInterceptors().add(new LoggingOutInterceptor());
    	factory.setServiceClass(HelloSEI.class);
    	factory.setBindingId("http://cxf.apache.org/bindings/xformat");
    	factory.setAddress("http://localhost:8080/WSScratch1/services/HelloWorld");
    	HelloSEI client = (HelloSEI) factory.create();

    	String output1 = client.sayHi("Rohit");
    	System.out.println("Server said: " + output1 + " THe END " );
    	System.exit(0);

    }

}
