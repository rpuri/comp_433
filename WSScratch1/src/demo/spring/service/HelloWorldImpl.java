package demo.spring.service;

import javax.jws.WebService;

@WebService(endpointInterface = "demo.spring.service.HelloSEI")
public class HelloWorldImpl implements HelloSEI {

    public String sayHi(String text) {
        System.out.println("sayHi called");
        return "Hello " + text;
    }
}

