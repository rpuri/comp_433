package demo.spring.service;

import javax.jws.WebService;

@WebService
public interface HelloSEI {

	String sayHi(String text);
}
